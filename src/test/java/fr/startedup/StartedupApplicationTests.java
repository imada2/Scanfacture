package fr.startedup;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import static io.restassured.RestAssured.given;
import fr.startedup.domain.Image;
import org.apache.commons.io.IOUtils;

@SpringBootTest
class StartedupApplicationTests {

    @Test
    public void convertImageToText() {
    	
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.put("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        Image image = new Image(); 
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("C:\\Users\\imad\\Desktop\\toto\\Scanfacture\\src\\test\\resources\\eurotext.png");
        
        image.setUserId("arun0009");
        image.setExtension(".png");
        
        try {
        	System.out.println("==========");
			image.setImage(Base64.encodeBase64(IOUtils.toByteArray(inputStream)));
			System.out.println(image);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        String response = given().contentType("application/json").headers(headers).body(image).when().post("http://localhost:9099/​scanImage").then()
                .statusCode(200).extract().response().body().asString();
        System.out.println("####### debut ###########");
        System.out.println(response);
        System.out.println("####### fin ###########");
    }
}
