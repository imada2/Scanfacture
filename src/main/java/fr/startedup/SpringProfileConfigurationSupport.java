package fr.startedup;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.core.env.Environment;

public final class SpringProfileConfigurationSupport {
	@SuppressWarnings("unused")
	private static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

	public static void addDefaultProfile(SpringApplication app) {
		Map<String, Object> defProperties = new HashMap<String, Object>();
		defProperties.put("spring.profiles.default", "dev");
		app.setDefaultProperties(defProperties);
	}

	public static String[] getActiveProfiles(Environment env) {
		String[] profiles = env.getActiveProfiles();
		if (profiles.length == 0) {
			return env.getDefaultProfiles();
		}
		return profiles;
	}
}