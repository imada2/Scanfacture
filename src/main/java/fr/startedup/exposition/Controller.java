package fr.startedup.exposition;

import java.io.File;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.startedup.domain.Image;
import fr.startedup.domain.Text;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

@RestController
public class Controller {

	private Logger LOGGER = LoggerFactory.getLogger(Controller.class);

	@GetMapping(value = "/")
	public String convertImageToText() {
		return "Hello world";
	}
	
	@RequestMapping(value = "/scanImage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Text convertImageToText(@RequestBody final Image image) throws Exception {

		System.out.println(image.getExtension());
		File tmpFile = File.createTempFile("ocr_image", image.getExtension());
		try {
			FileUtils.writeByteArrayToFile(tmpFile, Base64.decodeBase64(image.getImage()));
			Tesseract tesseract = new Tesseract(); // JNA Interface Mapping
			String imageText = tesseract.doOCR(tmpFile);
			LOGGER.debug("OCR Image Text = " + imageText);
			return new Text(imageText);
		} catch (Exception e) {
			LOGGER.error("Exception while converting/uploading image: ", e);
			throw new TesseractException();
		} finally {
			tmpFile.delete();
		}
	}

	@RequestMapping(value = "ocr/v1/convert", method = RequestMethod.GET)
	public Text convertImageToText(@RequestParam String url, @RequestParam(defaultValue = "png") String extension)
			throws Exception {
		File tmpFile = File.createTempFile("ocr_image", "." + extension);
		try {
			URLConnection conn = new URL(url).openConnection();
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0");
			conn.connect();
			FileUtils.copyInputStreamToFile(conn.getInputStream(), tmpFile);
			Tesseract tesseract = new Tesseract(); // JNA Interface Mapping
			tesseract.setDatapath("C:\\Users\\imad\\Desktop\\tessdata_best-master");
			String imageText = tesseract.doOCR(tmpFile);
			LOGGER.debug("OCR Image Text = " + imageText);
			return new Text(imageText);
		} catch (Exception e) {
			LOGGER.error("Exception while converting/uploading image: ", e);
			throw new TesseractException();
		} finally {
			tmpFile.delete();
		}
	}

	/*@PostMapping("postImage")
	public String postImg(@RequestParam("image") MultipartFile file) {
		try {
			if (file == null) {
				System.out.println("null file");
			} else {
				byte[] imageBytes = file.getBytes();
				ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes);
				BufferedImage bImage2 = ImageIO.read(bis);
				String path = "C:\\Users\\Home\\Documents\\OMG\\images\\" + file.getOriginalFilename();
				ImageIO.write(bImage2, "jpg", new File(path));
				OcrImage image = new OcrImage(1, "amal", path);
				ocrImageRepository.save(image);
				File fileImag = new File(image.getImgPath());
				ITesseract ins = new Tesseract();
				try {
					String result = ins.doOCR(fileImag);
					OcrText ocrText = new OcrText(1, 1, result);
					ocrTextController.saveOcrText(ocrText);
					return result;
				} catch (Exception e) {
					System.out.println("excep");
				}
			}
		} catch (Exception e) {
			System.out.println("exception");
		}
		return "";
	}*/

}
