package fr.startedup;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;

public abstract class AbstractSpringBootLauncher extends SpringBootServletInitializer {
	private static final Logger LOG = LoggerFactory.getLogger(AbstractSpringBootLauncher.class);

	@Autowired
	private Environment env;

	@PostConstruct
	public void initApplication() {
		LOG.info("Running with Spring profile(s) : {}", Arrays.toString(this.env.getActiveProfiles()));
		Collection<String> activeProfiles = Arrays.asList(this.env.getActiveProfiles());
		if ((activeProfiles.contains("activemq")) && (activeProfiles.contains("mq")))
			LOG.error(
					"Change the configuration of your application, it sould not run with both the 'mq' and 'activemq' profiles at the same time.");
	}

	public static void launch(Class<?> appClass, String[] args) throws UnknownHostException {
		SpringApplication app = new SpringApplication(new Class[] { appClass });
		SpringProfileConfigurationSupport.addDefaultProfile(app);
		Environment env = app.run(args).getEnvironment();
		LOG.info(
				"\n----------------------------------------------------------\n\tApplication {} is running! Access URLs:\n\tLocal: \t\thttp://localhost:{}\n\tExternal: \thttp://{}:{}\n----------------------------------------------------------",
				new Object[] { env.getProperty("spring.application.name"), env.getProperty("server.port"),
						InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port") });
	}
}
