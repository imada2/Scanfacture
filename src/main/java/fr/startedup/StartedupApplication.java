package fr.startedup;

import java.net.UnknownHostException;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class StartedupApplication extends AbstractSpringBootLauncher {

	public static void main(String[] args) throws UnknownHostException {
		AbstractSpringBootLauncher.launch(StartedupApplication.class, args);
	}

}
