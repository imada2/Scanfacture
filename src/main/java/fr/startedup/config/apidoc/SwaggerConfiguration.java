package fr.startedup.config.apidoc;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;

import fr.startedup.config.SuProperties;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import({ BeanValidatorPluginsConfiguration.class })
@Profile({ "swagger" })
public class SwaggerConfiguration {
	private static final Logger LOG = LoggerFactory.getLogger(SwaggerConfiguration.class);
	public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";

	@Bean
	public Docket swaggerSpringfoxDocket(SuProperties bddfProperties) {
		LOG.debug("Starting Swagger");
		StopWatch watch = new StopWatch();
		watch.start();
		Contact contact = new Contact(bddfProperties.getSwagger().getContactName(),
				bddfProperties.getSwagger().getContactUrl(), bddfProperties.getSwagger().getContactEmail());

		ApiInfoBuilder builder = new ApiInfoBuilder().title(bddfProperties.getSwagger().getTitle())
				.description(bddfProperties.getSwagger().getDescription())
				.version(bddfProperties.getSwagger().getVersion())
				.termsOfServiceUrl(bddfProperties.getSwagger().getTermsOfServiceUrl())
				.license(bddfProperties.getSwagger().getLicense())
				.licenseUrl(bddfProperties.getSwagger().getLicenseUrl());

		if (!IsNullOrEmpty(contact.getName())) {
			builder.contact(contact);
		}

		RequestParameterBuilder aChannelParameterBuilder = new RequestParameterBuilder();
		RequestParameterBuilder aUserIdParameterBuilder = new RequestParameterBuilder();
		RequestParameterBuilder aMediaParameterBuilder = new RequestParameterBuilder();

		aUserIdParameterBuilder.name("userId").in("header").required(false).build();
		aChannelParameterBuilder.name("channel").in("header").required(false).build();
		aMediaParameterBuilder.name("media").in("header").required(false).build();

		List aParameters = new ArrayList();
		aParameters.add(aMediaParameterBuilder.build());
		aParameters.add(aChannelParameterBuilder.build());
		aParameters.add(aUserIdParameterBuilder.build());

		Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(builder.build()).forCodeGeneration(true)
				.genericModelSubstitutes(new Class[] { ResponseEntity.class }).select()
				.paths(PathSelectors.regex("/api/.*")).build().globalRequestParameters(aParameters)
				.useDefaultResponseMessages(false);
		watch.stop();
		LOG.debug("Started Swagger in {} ms", Long.valueOf(watch.getTotalTimeMillis()));
		return docket;
	}

	public boolean IsNullOrEmpty(String param) {
		return param == null || param.trim().length() == 0;
	}
}