package fr.startedup.domain;

/**
 * @author imad
 *
 */
public class Remise {
	private String id;
	private String type;
	private double valeur;

	public Remise() {
		super();

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getValeur() {
		return valeur;
	}

	public void setValeur(double valeur) {
		this.valeur = valeur;
	}

}
