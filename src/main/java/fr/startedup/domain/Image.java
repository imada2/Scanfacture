package fr.startedup.domain;

/**
 * @author imad
 *
 */
public class Image {

	private String id;

	private String userId;

	private byte[] image;

	private String extension;

	private String text;

	public Image() {
		super();
	}

	public Image(String id, String userId, byte[] image, String extension, String text) {
		super();
		this.id = id;
		this.userId = userId;
		this.image = image;
		this.extension = extension;
		this.text = text;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return String.format("Image[userId=%s, image='%s', extenstion='%s', text='%s']", userId, image, extension,
				text);
	}
}