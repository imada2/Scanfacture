package fr.startedup.domain;

/**
 * @author imad
 *
 */
public class Info {
	private String id;
	private String type;
	private String value;

	public Info() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
