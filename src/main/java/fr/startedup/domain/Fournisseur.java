package fr.startedup.domain;

import java.util.List;

public class Fournisseur {

	private String id;
	private String reference;
	private String nom;
	private String email;
	private String telephone;
	private String fax;
	private String codeComptable;
	private String numTva;
	private Adresse adresse;
	private List<Contact> contacts;
	private List<Info> infos;

	public Fournisseur() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCodeComptable() {
		return codeComptable;
	}

	public void setCodeComptable(String codeComptable) {
		this.codeComptable = codeComptable;
	}

	public String getNumTva() {
		return numTva;
	}

	public void setNumTva(String numTva) {
		this.numTva = numTva;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public List<Info> getInfos() {
		return infos;
	}

	public void setInfos(List<Info> infos) {
		this.infos = infos;
	}

}
