package fr.startedup.domain;

public enum Role {

	ACCES_ALL, ACCES_CLIENT, ACCES_FRS, ACCES_USER
}
