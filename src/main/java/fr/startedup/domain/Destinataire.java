package fr.startedup.domain;

/**
 * @author imad
 *
 */
public class Destinataire {
	private String id;
	private String action;

	public Destinataire() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
