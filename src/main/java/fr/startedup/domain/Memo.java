package fr.startedup.domain;

/**
 * @author imad
 *
 */
public class Memo {

	private String id;
	private String value;

	public Memo() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
