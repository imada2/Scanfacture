package fr.startedup.domain;

import java.util.List;

/**
 * @author imad
 *
 */
public class Contact {
	private String id;
	private String civilite;
	private String nom;
	private String prenom;
	private String fonction;
	private String email;
	private String mobile;
	private String fixe;
	private List<Destinataire> destinataires;

	public Contact() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFixe() {
		return fixe;
	}

	public void setFixe(String fixe) {
		this.fixe = fixe;
	}

	public List<Destinataire> getDestinataires() {
		return destinataires;
	}

	public void setDestinataires(List<Destinataire> destinataires) {
		this.destinataires = destinataires;
	}

}
