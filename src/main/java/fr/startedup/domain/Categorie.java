package fr.startedup.domain;

/**
 * @author imad
 *
 */
public class Categorie {
	private String id;
	private String codeComptable;
	private String description;
	private String nom;

	public Categorie() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodeComptable() {
		return codeComptable;
	}

	public void setCodeComptable(String codeComptable) {
		this.codeComptable = codeComptable;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
