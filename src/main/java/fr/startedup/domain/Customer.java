package fr.startedup.domain;

import java.util.Date;
import java.util.List;

/**
 * @author imad
 *
 */
public class Customer {
	private String id;
	private String reference;
	private Boolean type;
	private String civilite;
	private String nom;
	private String email;
	private String telephone;
	private String fax;
	private String codeComptable;
	private String note;
	private List<Adresse> adresseFacturation;
	private List<Adresse> adresseLivraison;
	private List<Contact> contacts;
	private List<Info> infos;
	private String tauxRemise;
	private String conditions;
	private String delaiPaiement;
	private String identifiantLegal;
	private List<Historique> historiques;
	private List<Memo> memos;
	private Boolean isDeleted;
	private String searchTerms;
	private Date CreatedOn;
	private Date lastModifiedOn;

	public Customer() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Boolean getType() {
		return type;
	}

	public void setType(Boolean type) {
		this.type = type;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCodeComptable() {
		return codeComptable;
	}

	public void setCodeComptable(String codeComptable) {
		this.codeComptable = codeComptable;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<Adresse> getAdresseFacturation() {
		return adresseFacturation;
	}

	public void setAdresseFacturation(List<Adresse> adresseFacturation) {
		this.adresseFacturation = adresseFacturation;
	}

	public List<Adresse> getAdresseLivraison() {
		return adresseLivraison;
	}

	public void setAdresseLivraison(List<Adresse> adresseLivraison) {
		this.adresseLivraison = adresseLivraison;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public List<Info> getInfos() {
		return infos;
	}

	public void setInfos(List<Info> infos) {
		this.infos = infos;
	}

	public String getTauxRemise() {
		return tauxRemise;
	}

	public void setTauxRemise(String tauxRemise) {
		this.tauxRemise = tauxRemise;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public String getDelaiPaiement() {
		return delaiPaiement;
	}

	public void setDelaiPaiement(String delaiPaiement) {
		this.delaiPaiement = delaiPaiement;
	}

	public String getIdentifiantLegal() {
		return identifiantLegal;
	}

	public void setIdentifiantLegal(String identifiantLegal) {
		this.identifiantLegal = identifiantLegal;
	}

	public List<Historique> getHistoriques() {
		return historiques;
	}

	public void setHistoriques(List<Historique> historiques) {
		this.historiques = historiques;
	}

	public List<Memo> getMemos() {
		return memos;
	}

	public void setMemos(List<Memo> memos) {
		this.memos = memos;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getSearchTerms() {
		return searchTerms;
	}

	public void setSearchTerms(String searchTerms) {
		this.searchTerms = searchTerms;
	}

	public Date getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(Date createdOn) {
		CreatedOn = createdOn;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
}
