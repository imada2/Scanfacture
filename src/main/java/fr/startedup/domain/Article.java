package fr.startedup.domain;

/**
 * @author imad
 *
 */
public class Article {

	private String id;
	private String reference;
	private String designation;
	private String description;
	private String type;
	private Unite unite;
	private Categorie categorie;
	private Fournisseur fournisseur;
	private Remise remise;
	private double prixVenteTTC;
	private double prixAchatHt;
	private double prixVenteHt;
	private double taxe1;
	private double taxe2;

	public Article() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Unite getUnite() {
		return unite;
	}

	public void setUnite(Unite unite) {
		this.unite = unite;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public Remise getRemise() {
		return remise;
	}

	public void setRemise(Remise remise) {
		this.remise = remise;
	}

	public double getPrixVenteTTC() {
		return prixVenteTTC;
	}

	public void setPrixVenteTTC(double prixVenteTTC) {
		this.prixVenteTTC = prixVenteTTC;
	}

	public double getPrixAchatHt() {
		return prixAchatHt;
	}

	public void setPrixAchatHt(double prixAchatHt) {
		this.prixAchatHt = prixAchatHt;
	}

	public double getPrixVenteHt() {
		return prixVenteHt;
	}

	public void setPrixVenteHt(double prixVenteHt) {
		this.prixVenteHt = prixVenteHt;
	}

	public double getTaxe1() {
		return taxe1;
	}

	public void setTaxe1(double taxe1) {
		this.taxe1 = taxe1;
	}

	public double getTaxe2() {
		return taxe2;
	}

	public void setTaxe2(double taxe2) {
		this.taxe2 = taxe2;
	}

}
