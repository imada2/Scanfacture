package fr.startedup.domain;

import java.util.List;

/**
 * @author imad
 *
 */
public class Utilisateur {

	private String id;
	private String email;
	private String password;
	private Boolean adminPrincipal;
	private List<Utilisateur> utilisateurs;
	private List<Role> roles;

	public Utilisateur() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getAdminPrincipal() {
		return adminPrincipal;
	}

	public void setAdminPrincipal(Boolean adminPrincipal) {
		this.adminPrincipal = adminPrincipal;
	}

	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
}
