FROM openliberty/open-liberty:full-java11-openj9-ubi

# Add the application server configuration
#COPY --chown=1001:0  server.xml /config/

# For Open Liberty only
#RUN features.sh
EXPOSE 9080

# Add the application
COPY --chown=1001:0  target/startedup-0.0.1-SNAPSHOT.war /config/dropins/

#CMD ["/opt/ol/wlp/bin/server" "run" "defaultServer"]